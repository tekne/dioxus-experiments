use dioxus::prelude::*;

#[derive(Props, PartialEq)]
struct AppProps {
    logged_in: bool,
}

#[derive(Props, PartialEq)]
struct VoteButtonProps {
    score: i64,
}

#[allow(non_snake_case)]
fn VoteButton(cx: Scope<VoteButtonProps>) -> Element {
    let mut score = use_state(&cx, || cx.props.score);

    cx.render(rsx! {
        div {
            div { onclick: move|_| score += 1, "+" }
            div { "{score}" }
            div { onclick: move|_| score -= 1, "-" }
        }
    })
}

fn main() {
    dioxus::web::launch_with_props(app, AppProps { logged_in: false }, |config| config);
}

fn app(cx: Scope<AppProps>) -> Element {
    let names = [
        "myusername",
        "coolusername123",
        "nominalsets",
        "namey",
        "namelet",
        "nam",
    ];
    let name_list = names
        .iter()
        .map(|name| rsx!(li { key: "{name}", "{name}" }));
    if cx.props.logged_in {
        cx.render(rsx! {
            div { "logged in" }
        })
    } else {
        cx.render(rsx! {
            VoteButton { score: 50 }
            ul { name_list }
        })
    }
}
